package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

func main() {

	var kubeconfig *string
	if home := homeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}
	flag.Parse()

	// use the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		panic(err.Error())
	}

	// create the openshift clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

	// Create a Kubernetes core/v1 client.
	//coreclient, err := corev1client.NewForConfig(config)
	//if err != nil {
	//	panic(err)
	//}

	// List all persistent volumenes
	pv, err := clientset.CoreV1().PersistentVolumes().List(metav1.ListOptions{})

	// Set actual timestamp
	var tActual = time.Now()
	var tFuture = tActual.AddDate(0, 0, 30)

	//remove
	//var tFuture = tActual.Add(time.Second * time.Duration(1))
	//var annotationDelete = "manila.cloud-provider-openstack.marked-to-delete"

	annotationDelete := os.Getenv("VOLUME_RECLAIMER_DELETION_TIMESTAMP_ANNOTATION")

	for _, persV := range pv.Items {

		// Check status persistent volume
		pvStatus, err := clientset.CoreV1().PersistentVolumes().Get(persV.Name, metav1.GetOptions{})
		if err != nil {
			panic(err.Error())
		}

		if err != nil {
			panic(err.Error())
		}

		//  execute when PV name starts with `pvc-`
		if strings.HasPrefix(pvStatus.ObjectMeta.Name, "pvc-") {

			fmt.Printf("CEPHFS PersistenVolume: %s - Status: %s \n", persV.Name, pvStatus.Status.Phase)
			fmt.Println(pvStatus.ObjectMeta.Annotations[annotationDelete])

			fmt.Println(tActual.String())

			// if annotation does exists
			if _, ok := pvStatus.ObjectMeta.Annotations[annotationDelete]; !ok {

				// Set annotation with timestamp in PV
				fmt.Printf("Specifying annotations %s to persistent volume %s to be deleted into %s \n", annotationDelete, persV.Name, tFuture)

				patch := []byte(fmt.Sprintf(`{"metadata": {"annotations": {"%s": "%s"}}}`, annotationDelete, tActual))
				ok, err := clientset.CoreV1().PersistentVolumes().Patch(persV.Name, types.StrategicMergePatchType, patch)
				if err != nil {
					fmt.Print(ok)
				}

				// execute when timestamt is higher than tFuture
			} else if tActual.String() >= pvStatus.ObjectMeta.Annotations[annotationDelete] {
				fmt.Printf("PV '%s' is marked for deletion on the '%s', which has already passed. Proceeding with the deletion ...", persV.Name, pvStatus.ObjectMeta.Annotations[annotationDelete])

				patch := []byte(`{"spec": {"persistentVolumeReclaimPolicy": "Delete"}}`)
				_, err = clientset.CoreV1().PersistentVolumes().Patch(persV.Name, types.StrategicMergePatchType, patch)
				fmt.Printf("PV '%s' successfully deleted and unprovisioned!", persV.Name)
			} else {
				fmt.Printf("FAILURE to delete the Kubernetes PV %s - Status: %s", persV.Name, pvStatus.Status.Phase)
			}
		}
	}
	fmt.Printf("All existing PersistentVolumes have been processed")
}

func homeDir() string {
	if h := os.Getenv("HOME"); h != "" {
		return h
	}
	return os.Getenv("USERPROFILE") // windows
}
